package project;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class BoardInterface extends JFrame implements ActionListener{

	private JPanel panel;
	Run  Runner ; 
	boolean end  = false ;
	boolean  Player = false ; 
	boolean Single = false ;
	boolean Multi  = false ; 
	StopWatch CountDown = new StopWatch() ; 
	boolean started  = false; 
	private ImageIcon emptyIcon = new ImageIcon("resources/Empty.JPG");
	private ImageIcon whiteIcon = new ImageIcon("resources/White.JPG");
	private ImageIcon blackIcon = new ImageIcon("resources/Black.JPG");

	private JButton button[][] = new JButton[8][8];
	private JButton tempButton;
	private JLabel score,black,white,turn;
	private int whiteScore,blackScore;
	Vector<Map> availableCells = new Vector<Map>() ; 



	public BoardInterface(String type)
	{
		this.setSize(368,460);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setTitle("Revesi");

		panel = new JPanel();
		panel.setSize(400,400);
		panel.setLayout(null);
		panel.setBackground(Color.ORANGE);


		for(int i=0 ; i<8 ; i++)
		{
			for(int j=0 ; j<8 ; j++)
			{
				tempButton = new JButton();
				tempButton.setSize(45, 45);
				tempButton.setLocation((j*45), (i*45));

				if((i== 3 && j == 3)||(i==4 && j==4))
					tempButton.setIcon(whiteIcon);
				else if((i==3 && j == 4)||(i == 4&& j == 3))
					tempButton.setIcon(blackIcon);
				else
					tempButton.setIcon(emptyIcon);

				button[i][j] = new JButton();
				button[i][j] = tempButton;
				panel.add(button[i][j]);
				button[i][j].setActionCommand("" + i + "" + j);
				button[i][j].addActionListener(this);
			}
		}
		score = new JLabel("Score:");
		score.setSize(50, 50);
		score.setLocation(20, 360);

		black = new JLabel("black: 2");
		black.setSize(60,60);
		black.setLocation(70, 370);

		white = new JLabel("white: 2");
		white.setSize(60, 60);
		white.setLocation(70, 390);
		white.setForeground(Color.WHITE);

		turn = new JLabel("Turn: black");
		turn.setSize(100, 50);
		turn.setLocation(200, 360);

		panel.add(score);
		panel.add(black);
		panel.add(white);
		panel.add(turn);

		this.add(panel);
		this.setVisible(true);
		this.setLocationRelativeTo(null);
		Runner = new Run () ; 
		Runner.initializethegrid();
	
		if (type.equals("Single") )
			Single = true ; 
		else Multi = true ; 
		
		
	}

	
	
	public void actionPerformed(ActionEvent e) 
	{    

		if (Multi){
			boolean check =   false ;  
			String command = e.getActionCommand();
			int row = Integer.parseInt("" + command.charAt(0));
			int col = Integer.parseInt("" + command.charAt(1));
			System.out.println(row + " "+ col);
			if (Player  == false){
				check =	Runner.checkvalid(row, col, '0')  ;	
				if (check)
					Player = true  ; 			
			}
			else {	
				check =	Runner.checkvalid(row, col, '1')  ;	
				if (check)
					Player= false ; 
			}

			if (check){
				updateBoard();
				updateScore();

			}
		}
		
		
		
		else if (Single){
			boolean check =   false ;  
			String command = e.getActionCommand();
			int row = Integer.parseInt("" + command.charAt(0));
			int col = Integer.parseInt("" + command.charAt(1));
			System.out.println(row + " "+ col);
			check =	Runner.checkvalid(row, col, '0')  ;	
			
			if (check){			
				Single() ; 				
				updateBoard();
				updateScore();							
			}		
			
		}
	}
	
	
	

	
	
	public void Single (){
			for (int i = 0 ; i < 8  ;  i ++){				
				for (int j = 0; j < 8  ; j ++){					
					if (Runner.check_valid_cmp(i, j, '1') != -1){						
						Map temp = new Map() ; 		
						temp.cordinates.first = i ; 
						temp.cordinates.second = j ; 
						temp.counter= Runner.check_valid_cmp(i, j, '1') ; 
						availableCells.add(temp) ; 
					}
				}				
			}

			

			if (availableCells.size() != 0){
				Runner.checkvalid(availableCells.get(availableCells.size() - 1).cordinates.first,availableCells.get(availableCells.size() - 1).cordinates.second, '1') ; 
				updateBoard();
				updateScore();
				
			}
		}
				
	
	public void updateBoard()
	{
		for(int i=0 ; i<8 ; i++)
		{
			for(int j=0 ; j<8 ; j++)
			{
				if(Runner.Grid[i][j] == '1')
					button[i][j].setIcon(whiteIcon);
				else if(Runner.Grid[i][j] == '0')
					button[i][j].setIcon(blackIcon);
			}
		}


		if(Player == true )

		{	turn.setText("Turn: white");

		turn.setForeground(Color.white);				


		}
		else
		{
			turn.setText("Turn: black");
			turn.setForeground(Color.BLACK);

		}

		if(Runner.Counter().first  + Runner.Counter().second == 64 )
			end= true ; 
	}





	public void updateScore()
	{
		whiteScore = 0;
		blackScore = 0;

		for(int i=0 ; i<8 ; i++)
		{
			for(int j=0 ; j<8 ; j++)
			{
				if(button[i][j].getIcon()== whiteIcon)
					whiteScore++;
				else if(button[i][j].getIcon()== blackIcon)
					blackScore++;
			}
		}
		white.setText("white :"+String.valueOf(whiteScore));
		black.setText("black :"+String.valueOf(blackScore));

		if(end == true)
		{
			if(whiteScore > blackScore)
				JOptionPane.showMessageDialog(null, "winner: white");
			else if(blackScore > whiteScore)
				JOptionPane.showMessageDialog(null, "winner: black");
			else
				JOptionPane.showMessageDialog(null, "draw!");

			JOptionPane.showMessageDialog(null, "Game over.");
			this.dispose();
		}
	}

}