package project;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MainMenu extends JFrame implements ActionListener {

	private JPanel panel;
	private JButton multButton;
	private JButton singleButton;
	private JLabel label;
	private ImageIcon singleImage = new ImageIcon("resources/single.JPG");
	private ImageIcon multImage = new ImageIcon("resources/multi.JPG");
	ImageIcon background = new ImageIcon("resources/background.JPG");
	//private Player p;
	
	public MainMenu ()
	{
		this.setSize(368,393);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setTitle("Reversi");
		
		
		JLabel label = new JLabel();
		label.setIcon(background);
		label.setSize(368,393);
		
		singleButton = new JButton();
		singleButton.setSize(225, 45);
		singleButton.setLocation(70, 260);
		singleButton.setIcon(singleImage);
		
		multButton = new JButton();
		multButton.setSize(225, 45);
		multButton.setLocation(70, 310);
		multButton.setIcon(multImage);
		
		panel = new JPanel();
		panel.setLayout(null);
		panel.add(singleButton);
		panel.add(multButton);
		panel.add(label);
		
		this.add(panel);
		this.setLocationRelativeTo(null);
		
		multButton.addActionListener(this);
		singleButton.addActionListener(this);
		
		this.setVisible(true);
	}
	
	//@Override
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource() == multButton)
		{
			this.setVisible(false);
			BoardInterface board = new BoardInterface("multi");
			this.dispose();
			
		}
		else {
			this.setVisible(false);
			BoardInterface board = new BoardInterface("Single");
			this.dispose();	
		}
		
	}

}
