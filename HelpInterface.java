import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class HelpInterface extends JFrame {
	private JLabel label;
	private JLabel headerLabel;
	private JPanel panel;
	private ImageIcon background = new ImageIcon("resources/help background.JPG");
	
	HelpInterface()
	{
		this.setSize(368,560);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setResizable(false);
		this.setTitle("Help");
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		
		
		label = new JLabel();
		label.setIcon(background);
		label.setSize(368,545);
		
		headerLabel = new JLabel();
		headerLabel.setText("How to play the game :");
		headerLabel.setSize(100, 100);
		headerLabel.setLocation(100, 100);
		
		panel = new JPanel();
		panel.add(label);
		panel.add(headerLabel);
		
		this.add(panel);
	}

}
